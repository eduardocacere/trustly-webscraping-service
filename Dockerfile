FROM adoptopenjdk/openjdk11:alpine

ENV APP_PORT=8080

RUN mkdir app/
COPY target/trustly-webscraping-0.0.1.jar app/

CMD java -jar -Dserver.port=${APP_PORT} app/trustly-webscraping-0.0.1.jar
