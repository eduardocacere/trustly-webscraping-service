# What is this service ?
This is a service that performs data extraction using Web Scraping techniques.

This service is extracting data from Github repositories.

Using NoSql Database to persist data.


# How it works?
This service has 2 endpoints.
A GET request that performs a summary of the extracted data.
Another POST method that receives the name of the repository and performs data extractions.
These APIs can be accessed through the url: `/swagger-ui.html`

# How to run locally ?

## Requirements:

1. JDK 11
2. Docker

## Step by step
1. Climb the local infrastructure running: `docker-compose -f docker-compose-local.yml up -d`. A MongoDb used to persist the data will be executed.

### Dependencies:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.0/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.0/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.5.0/reference/htmlsingle/#boot-features-developing-web-applications)
* [MongoDb](https://docs.mongodb.com/manual/tutorial/getting-started/)
