package com.trustly.webscraping.services;

import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.domain.SummaryRepo;
import com.trustly.webscraping.dto.SummaryDataDto;
import com.trustly.webscraping.enums.StatusSync;
import com.trustly.webscraping.repository.RepoDocumentRepository;
import com.trustly.webscraping.services.Impl.WebScrapingGithubServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collections;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WebScrapingGithubServiceImplTest {

    private RepoDocumentRepository repoDocumentRepository;
    private PageRepositoryGithubService pageRepositoryGithubService;
    private ProcessRepoService pageAboutProjectService;
    private ProcessRepoService pageCodeFileService;

    private WebScrapingGithubServiceImpl webScrapingGithubService;

    @BeforeEach
    void setUp() {
        repoDocumentRepository = mock(RepoDocumentRepository.class);
        pageRepositoryGithubService = mock(PageRepositoryGithubService.class);
        pageAboutProjectService = mock(ProcessRepoService.class);
        pageCodeFileService = mock(ProcessRepoService.class);

        this.webScrapingGithubService = new WebScrapingGithubServiceImpl(repoDocumentRepository, pageRepositoryGithubService, pageCodeFileService, pageCodeFileService);
    }

    @Test
    public void generateSummaryTest() {
        Repo repo = Repo.builder().statusSync(StatusSync.COMPLETE).build();
        SummaryRepo summaryRepo = SummaryRepo.builder().size(BigDecimal.ONE).build();

        when(this.repoDocumentRepository.findByNameRepository(anyString())).thenReturn(Collections.singletonList(repo));
        when(this.repoDocumentRepository.summaryRepo(anyString())).thenReturn(Collections.singletonList(summaryRepo));

        SummaryDataDto summaryData = this.webScrapingGithubService.generateSummary(anyString());
        assertEquals(summaryData.getStatusSync(), StatusSync.COMPLETE);
    }

    @Test
    public void generateSummaryInProgressWithPathFileNullTest() {
        Repo repo = Repo.builder().statusSync(StatusSync.IN_PROGRESS).build();
        SummaryRepo summaryRepo = SummaryRepo.builder().size(BigDecimal.ONE).build();

        when(this.repoDocumentRepository.findByNameRepository(anyString())).thenReturn(Collections.singletonList(repo));
        when(this.repoDocumentRepository.summaryRepo(anyString())).thenReturn(Collections.singletonList(summaryRepo));

        SummaryDataDto summaryData = this.webScrapingGithubService.generateSummary(anyString());
        assertEquals(summaryData.getStatusSync(), StatusSync.COMPLETE);
    }

    @Test
    public void generateSummaryInProgressTest() {
        Repo repo = Repo.builder()
                .statusSync(StatusSync.IN_PROGRESS)
                .pathFiles(asList())
                .build();
        SummaryRepo summaryRepo = SummaryRepo.builder().size(BigDecimal.ONE).build();

        when(this.repoDocumentRepository.findByNameRepository(anyString())).thenReturn(Collections.singletonList(repo));
        when(this.repoDocumentRepository.summaryRepo(anyString())).thenReturn(Collections.singletonList(summaryRepo));

        SummaryDataDto summaryData = this.webScrapingGithubService.generateSummary(anyString());
        assertEquals(summaryData.getStatusSync(), StatusSync.IN_PROGRESS);
    }

    @Test
    public void generateSummaryNotFoundRepositoryTest() {
        when(this.repoDocumentRepository.findByNameRepository(anyString())).thenReturn(asList());

        SummaryDataDto summaryData = this.webScrapingGithubService.generateSummary(anyString());
        assertNull(summaryData.getStatusSync());
    }

}
