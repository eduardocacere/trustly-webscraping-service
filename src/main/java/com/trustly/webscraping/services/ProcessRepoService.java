package com.trustly.webscraping.services;

import com.trustly.webscraping.domain.Repo;

public interface ProcessRepoService {

    void execute(Repo repo);
}
