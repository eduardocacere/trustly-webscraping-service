package com.trustly.webscraping.services;

import com.trustly.webscraping.domain.Repo;

public interface CountLinesProjectService {

    void execute(Repo repo);
}
