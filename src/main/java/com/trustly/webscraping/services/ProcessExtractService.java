package com.trustly.webscraping.services;

import com.trustly.webscraping.domain.Repo;

public interface ProcessExtractService {

    void execute(String page, Repo repo);
}
