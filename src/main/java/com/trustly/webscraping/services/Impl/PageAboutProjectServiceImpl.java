package com.trustly.webscraping.services.Impl;

import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.proxy.GithubProxy;
import com.trustly.webscraping.services.ProcessExtractService;
import com.trustly.webscraping.services.ProcessRepoService;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("PageAboutProjectService")
@Slf4j
public class PageAboutProjectServiceImpl implements ProcessRepoService {

    private final GithubProxy githubProxy;
    private final ProcessExtractService processBranchService;
    private final ProcessExtractService processLanguageService;

    public PageAboutProjectServiceImpl(GithubProxy githubProxy,
                                       @Qualifier("ProcessBranchService") ProcessExtractService processBranchService,
                                       @Qualifier("ProcessLanguageService") ProcessExtractService processLanguageService) {
        this.githubProxy = githubProxy;
        this.processBranchService = processBranchService;
        this.processLanguageService = processLanguageService;
    }

    @Override
    public void execute(Repo repo) {
        try {
            String page = this.githubProxy.getPage(repo.getPath());
            this.processBranchService.execute(page, repo);
            this.processLanguageService.execute(page, repo);

        }catch (FeignException fex) {
            log.error("There was an error in the request. {}", fex.getMessage());
        }



    }
}
