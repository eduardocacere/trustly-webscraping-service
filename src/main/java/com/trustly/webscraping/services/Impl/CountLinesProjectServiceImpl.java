package com.trustly.webscraping.services.Impl;

import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.services.ProcessExtractService;
import com.trustly.webscraping.util.ExtractServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("CountLinesProjectService")
@Slf4j
@RequiredArgsConstructor
public class CountLinesProjectServiceImpl implements ProcessExtractService {

    private static final String REGEX_COUNT = "(.* lines)";
    private static final Long VALUE_DEFAULT = 0l;

    @Override
    public void execute(String page, Repo repo) {
        log.info("Counting Lines project: {}", repo.getNameProject());
        this.counting(repo, page);
        log.info("Total Lines: {}", repo.getCodeLine());
    }

    private void counting(Repo repo, String page) {

        Long lines = ExtractServiceUtil.extract(page, REGEX_COUNT)
                .map(m -> m.group(1))
                .map(m -> m.replace("lines", ""))
                .map(String::trim)
                .map(Long::valueOf)
                .orElse(VALUE_DEFAULT);

        Long repoLine = Optional.ofNullable(repo.getCodeLine()).orElse(VALUE_DEFAULT);
        repoLine += lines;
        repo.setCodeLine(repoLine);
    }
}
