package com.trustly.webscraping.services.Impl;

import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.dto.UrlFileDto;
import com.trustly.webscraping.proxy.GithubProxy;
import com.trustly.webscraping.repository.RepoDocumentRepository;
import com.trustly.webscraping.services.ProcessExtractService;
import com.trustly.webscraping.util.ExtractServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("ProcessLoadListFileService")
@Slf4j
@RequiredArgsConstructor
public class ProcessLoadListFileService implements ProcessExtractService {

    private final RepoDocumentRepository repoDocumentRepository;
    private final GithubProxy githubProxy;

    private static final String REGEX_LIST_FILE_CODE = "data-url=\\\"([^\\\"]*)\\\"";

    @Value("${web-scraping.header.x-requested-with}")
    private String xRequestedWith;

    @Override
    public void execute(String page, Repo repo) {
        ExtractServiceUtil.extract(page, REGEX_LIST_FILE_CODE)
                .map(i -> i.group(1))
                .ifPresent(path -> this.updateFileList(this.githubProxy.getListUrlFile(path, this.xRequestedWith), repo));
    }

    private void updateFileList(UrlFileDto listUrlFile, Repo repo) {
        repo.setPathFiles(listUrlFile.getPaths());
        this.repoDocumentRepository.save(repo);
    }
}
