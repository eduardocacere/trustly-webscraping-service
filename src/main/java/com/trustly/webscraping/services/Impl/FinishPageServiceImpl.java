package com.trustly.webscraping.services.Impl;

import com.trustly.webscraping.services.ExtractInfoService;
import com.trustly.webscraping.util.ExtractServiceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("FinishPageService")
@Slf4j
public class FinishPageServiceImpl implements ExtractInfoService {

    private static final String REGEX = "This organization has no more repositories.";

    @Override
    public String extract(String page) {
        return ExtractServiceUtil.extract(page, REGEX)
                .map(i -> i.group(0))
                .orElse("");
    }
}
