package com.trustly.webscraping.services.Impl;

import com.trustly.webscraping.services.ExtractInfoService;
import com.trustly.webscraping.util.ExtractServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("ExtractUrlRepoService")
@Slf4j
@RequiredArgsConstructor
public class ExtractUrlRepoService implements ExtractInfoService {

    private static final String REGEX = "href=\\\"([^\\\"]*)\\\"";

    @Override
    public String extract(String page) {
        return ExtractServiceUtil.extract(page, REGEX)
                .map(m -> m.group(1))
                .orElse("");
    }
}
