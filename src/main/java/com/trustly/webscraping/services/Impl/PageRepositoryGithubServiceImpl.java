package com.trustly.webscraping.services.Impl;

import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.proxy.GithubProxy;
import com.trustly.webscraping.services.PageRepositoryGithubService;
import com.trustly.webscraping.services.LoadRepoService;
import com.trustly.webscraping.util.ExtractServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class PageRepositoryGithubServiceImpl implements PageRepositoryGithubService {

    private final GithubProxy githubProxy;
    private final LoadRepoService loadRepoService;

    private static final int PAGE_INITIALIZE = 1;
    private static final String REGEX_FINISH = "This organization has no more repositories.";

    @Override
    public void load(String repositoryName) {
        int pageIndice = PAGE_INITIALIZE;
        Boolean finish = Boolean.TRUE;
        List<Repo> repos = new ArrayList<>();

        while (finish) {
            String page = this.githubProxy.getRepository(repositoryName, pageIndice);
            if(this.isFinishPage(page)) {
                break;
            }
            this.loadRepoService.load(repositoryName, page, repos);
            pageIndice += 1;
        }
    }

    private Boolean isFinishPage(String page) {
        return ExtractServiceUtil.extract(page, REGEX_FINISH)
                .map(m -> m.group(0))
                .map(i -> Boolean.TRUE)
                .orElse(Boolean.FALSE);
    }
}
