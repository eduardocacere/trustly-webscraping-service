package com.trustly.webscraping.services.Impl;

import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.proxy.GithubProxy;
import com.trustly.webscraping.repository.RepoDocumentRepository;
import com.trustly.webscraping.services.ProcessExtractService;
import com.trustly.webscraping.services.ProcessRepoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("PageCodeFileService")
@Slf4j
public class PageCodeFileServiceImpl implements ProcessRepoService {

    private final ProcessExtractService countLinesProjectService;
    private final ProcessExtractService sizeFileCodeProjectService;
    private final RepoDocumentRepository repoDocumentRepository;
    private final GithubProxy githubProxy;

    public PageCodeFileServiceImpl(@Qualifier("CountLinesProjectService") ProcessExtractService countLinesProjectService,
                                   @Qualifier("SizeFileCodeProjectService") ProcessExtractService sizeFileCodeProjectService,
                                   RepoDocumentRepository repoDocumentRepository,
                                   GithubProxy githubProxy) {
        this.countLinesProjectService = countLinesProjectService;
        this.sizeFileCodeProjectService = sizeFileCodeProjectService;
        this.repoDocumentRepository = repoDocumentRepository;
        this.githubProxy = githubProxy;
    }

    @Override
    public void execute(Repo repo) {

        repo.getPathFiles()
                .stream()
                .filter(f -> !repo.getBranch().isEmpty())
                .forEach(f -> {
                    String url = mountUrl(repo, f);
                    String page = this.getPage(url);
                    this.countLinesProjectService.execute(page, repo);
                    this.sizeFileCodeProjectService.execute(page, repo);
                    this.repoDocumentRepository.save(repo);
                });

    }

    private String mountUrl(Repo repo, String pathFile) {
        return repo.getPath()
                .concat("/blob/")
                .concat(repo.getBranch())
                .concat("/")
                .concat(pathFile);
    }

    private String getPage(String url) {
        return this.githubProxy.getPage(url);
    }
}
