package com.trustly.webscraping.services.Impl;


import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.enums.StatusSync;
import com.trustly.webscraping.repository.RepoDocumentRepository;
import com.trustly.webscraping.services.LoadRepoService;
import com.trustly.webscraping.util.ExtractServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Matcher;


@Service
@Slf4j
@RequiredArgsConstructor
public class ExtractProjectGithubServiceImpl implements LoadRepoService {

    private final RepoDocumentRepository repoDocumentRepository;
    private final ExtractUrlRepoService extractUrlRepoService;
    private static final String REGEX = "<h3 \\b[^>]+>(\\s|\\b|\\n)+<a .*?(\\b[^>]+)>(\\s|\\b|\\n)+(\\S+)([\\s]|)</a>";

    @Override
    public void load(String repository, String page, List<Repo> repos) {

        ExtractServiceUtil.extract(page, REGEX)
                .ifPresent(matcher -> this.loadData(matcher, repository));
    }

    private void loadData(Matcher matcher, String repository) {
        while (matcher.find()) {
            Repo repo = Repo.builder()
                    .nameRepository(repository)
                    .nameProject(matcher.group(4))
                    .path(this.extractUrlRepoService.extract(matcher.group(2)))
                    .statusSync(StatusSync.IN_PROGRESS)
                    .dateCreate(LocalDateTime.now())
                    .build();
            this.repoDocumentRepository.findByNameProject(repo.getNameProject())
                    .ifPresentOrElse(
                            r -> {
                                r.setStatusSync(StatusSync.IN_PROGRESS);
                                this.repoDocumentRepository.save(r);
                            },
                            () -> this.repoDocumentRepository.save(repo)
                    );
        }

    }
}
