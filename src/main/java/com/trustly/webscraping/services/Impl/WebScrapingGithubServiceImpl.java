package com.trustly.webscraping.services.Impl;

import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.domain.SummaryRepo;
import com.trustly.webscraping.dto.SummaryDataDto;
import com.trustly.webscraping.enums.StatusSync;
import com.trustly.webscraping.repository.RepoDocumentRepository;
import com.trustly.webscraping.services.PageRepositoryGithubService;
import com.trustly.webscraping.services.ProcessRepoService;
import com.trustly.webscraping.services.WebScrapingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Service
@Slf4j
public class WebScrapingGithubServiceImpl implements WebScrapingService {


    private final RepoDocumentRepository repoDocumentRepository;
    private final PageRepositoryGithubService pageRepositoryGithubService;
    private final ProcessRepoService pageAboutProjectService;
    private final ProcessRepoService pageCodeFileService;

    public WebScrapingGithubServiceImpl(RepoDocumentRepository repoDocumentRepository,
                                        PageRepositoryGithubService pageRepositoryGithubService,
                                        @Qualifier("PageAboutProjectService") ProcessRepoService pageAboutProjectService,
                                        @Qualifier("PageCodeFileService") ProcessRepoService pageCodeFileService) {
        this.repoDocumentRepository = repoDocumentRepository;
        this.pageRepositoryGithubService = pageRepositoryGithubService;
        this.pageAboutProjectService = pageAboutProjectService;
        this.pageCodeFileService = pageCodeFileService;
    }

    private static final String REPO_NOT_FOUND = "Repository not found. Use the creation endpoint to load the data.";
    private static final String REPO_IN_PROGRESS = "The repository is being updated. We currently have this information.";
    private static final String REPO_COMPLETE = "Repository has updated information.";
    private static final String LANGUAGE_NOT_DEFINED = "language not defined";

    @Override
    public SummaryDataDto generateSummary(String repository) {
        log.info("Generate Summary");

        List<Repo> repos = this.repoDocumentRepository.findByNameRepository(repository);
        if (repos.isEmpty()) {
            log.info("Repo not found.");
            return this.summaryNotFound();
        }
        Map<String, Object> infoSummary = this.prepareInfoSummary(repos);
        return this.mountSummary(infoSummary, repository);
    }

    @Override
    @Async
    public void createDataRepo(String repository) {
        log.info("Starting the process of loading repository information");

        this.pageRepositoryGithubService.load(repository);
        List<Repo> repos = this.repoDocumentRepository.findByNameRepository(repository);

        repos.forEach( r -> {
            this.pageAboutProjectService.execute(r);
            this.pageCodeFileService.execute(r);
            r.setStatusSync(StatusSync.COMPLETE);
            this.repoDocumentRepository.save(r);
        });

        log.info("Finish the process of loading repository {} information", repository);
    }

    private Map<String, Object> prepareInfoSummary(List<Repo> repos) {
        Map<String, Object> infos = new HashMap<>();
        if (repos.stream().noneMatch(i -> i.getStatusSync().equals(StatusSync.IN_PROGRESS) && Optional.ofNullable(i.getPathFiles()).isPresent())) {
            infos.put("status", StatusSync.COMPLETE);
            infos.put("details", REPO_COMPLETE);
            return infos;
        }
        infos.put("status", StatusSync.IN_PROGRESS);
        infos.put("details", REPO_IN_PROGRESS);
        return infos;
    }
    private SummaryDataDto mountSummary(Map<String, Object> infoSummary, String repository) {
        List<SummaryRepo> summaryRepos = this.repoDocumentRepository.summaryRepo(repository)
                .stream()
                .map(i -> {
                    i.setLanguage(Optional.ofNullable(i.getLanguage()).orElse(LANGUAGE_NOT_DEFINED));
                    i.setSize(i.getSize().setScale(2, RoundingMode.HALF_UP));
                    return i;
                }).collect(Collectors.toList());

        log.info("End Generate Summary.");
        return SummaryDataDto
                .builder()
                .statusSync((StatusSync) infoSummary.get("status"))
                .details((String) infoSummary.get("details"))
                .summary(summaryRepos)
                .build();
    }

    private SummaryDataDto summaryNotFound() {
        return SummaryDataDto
                .builder()
                .statusSync(null)
                .details(REPO_NOT_FOUND)
                .summary(asList())
                .build();
    }
}
