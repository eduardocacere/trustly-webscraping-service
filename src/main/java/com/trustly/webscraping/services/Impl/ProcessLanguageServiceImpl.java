package com.trustly.webscraping.services.Impl;

import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.repository.RepoDocumentRepository;
import com.trustly.webscraping.services.ProcessExtractService;
import com.trustly.webscraping.util.ExtractServiceUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service("ProcessLanguageService")
@Slf4j
@RequiredArgsConstructor
public class ProcessLanguageServiceImpl implements ProcessExtractService {

    private final RepoDocumentRepository repoDocumentRepository;
    private static final String REGEX_LANGUAGE = "color-text-primary text-bold mr-1.>([^<]+)";

    @Override
    public void execute(String page, Repo repo) {
        log.info("Start search language project");
        ExtractServiceUtil.extract(page, REGEX_LANGUAGE)
                .map(m -> m.group(1))
                .ifPresent(language -> {
                    log.info("Language Project: {}", language);
                    repo.setLanguage(language);
                    this.repoDocumentRepository.save(repo);
                });
    }
}
