package com.trustly.webscraping.services.Impl;

import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.proxy.GithubProxy;
import com.trustly.webscraping.repository.RepoDocumentRepository;
import com.trustly.webscraping.services.ProcessExtractService;
import com.trustly.webscraping.util.ExtractServiceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("ProcessBranchService")
@Slf4j
public class ProcessBranchServiceImpl implements ProcessExtractService {

    private final GithubProxy githubProxy;
    private final RepoDocumentRepository repoDocumentRepository;
    private final ProcessExtractService processLoadListFileService;
    private static final String REGEX_REPO_BRANCH = "(data-menu-button).([\\S+]+)<";

    public ProcessBranchServiceImpl(GithubProxy githubProxy,
                                    RepoDocumentRepository repoDocumentRepository,
                                    @Qualifier("ProcessLoadListFileService") ProcessExtractService processLoadListFileService) {
        this.githubProxy = githubProxy;
        this.repoDocumentRepository = repoDocumentRepository;
        this.processLoadListFileService = processLoadListFileService;
    }

    @Override
    public void execute(String page, Repo repo) {
        ExtractServiceUtil.extract(page, REGEX_REPO_BRANCH)
                .map(m -> m.group(2))
                .ifPresent(branchName -> {
                    String branch = branchName.replace("\"\">", "");
                    String pageListFileCode = this.githubProxy.getListFileCode(repo.getNameRepository(), repo.getNameProject(), branch);
                    repo.setBranch(branch);
                    this.repoDocumentRepository.save(repo);
                    this.processLoadListFileService.execute(pageListFileCode, repo);
                });
    }
}
