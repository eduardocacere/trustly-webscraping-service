package com.trustly.webscraping.services.Impl;

import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.services.ProcessExtractService;
import com.trustly.webscraping.util.ExtractServiceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("SizeFileCodeProjectService")
@Slf4j
public class SizeFileCodeProjectServiceImpl implements ProcessExtractService {

    private static final String REGEX_SIZE = "(.* KB)|(.* Bytes)";
    private static final Double VALUE_DEFAULT = 0d;

    public SizeFileCodeProjectServiceImpl() {
    }

    @Override
    public void execute(String page, Repo repo) {
        log.info("Size project: {}", repo.getNameProject());
        this.size(repo, page);
        log.info("Total size: {}", repo.getCodeLine());
    }

    private void size(Repo repo, String page) {
        Double size = ExtractServiceUtil.extract(page, REGEX_SIZE)
                .map(m -> m.group(1))
                .map(m -> m.replace("Bytes", ""))
                .map(m -> m.replace("KB", ""))
                .map(String::trim)
                .map(Double::valueOf)
                .orElse(VALUE_DEFAULT);
        Double repoSize = Optional.ofNullable(repo.getSize()).orElse(VALUE_DEFAULT);
        repoSize += size;
        repo.setSize(repoSize);
    }
}
