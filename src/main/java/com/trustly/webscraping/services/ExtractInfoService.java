package com.trustly.webscraping.services;

public interface ExtractInfoService {

    String extract(String page);
}
