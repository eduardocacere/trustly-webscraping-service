package com.trustly.webscraping.services;

import com.trustly.webscraping.domain.Repo;

import java.util.List;

public interface LoadRepoService {

    void load(String repository, String page, List<Repo> repos);
}
