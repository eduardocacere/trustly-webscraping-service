package com.trustly.webscraping.services;

public interface PageRepositoryGithubService {

    void load(String repositoryName);
}
