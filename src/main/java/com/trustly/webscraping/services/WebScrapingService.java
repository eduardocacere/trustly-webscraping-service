package com.trustly.webscraping.services;

import com.trustly.webscraping.dto.SummaryDataDto;

public interface WebScrapingService {

    SummaryDataDto generateSummary(String repository);
    void createDataRepo(String repository);
}
