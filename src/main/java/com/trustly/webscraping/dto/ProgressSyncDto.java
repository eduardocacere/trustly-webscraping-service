package com.trustly.webscraping.dto;

import com.trustly.webscraping.enums.StatusSync;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProgressSyncDto {

    @Schema(name = "statusSync", description = "Repository data synchronization status.")
    private StatusSync statusSync;

    @Schema(name = "message", description = "Message of the repository synchronization process.")
    private String message;

}
