package com.trustly.webscraping.dto;

import com.trustly.webscraping.domain.SummaryRepo;
import com.trustly.webscraping.enums.StatusSync;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SummaryDataDto {

    @Schema(name = "statusSync", description = "Repository data synchronization status.")
    StatusSync statusSync;

    @Schema(name = "details", description = "Details of the repository synchronization process.")
    String details;

    @Schema(name = "summary", description = "List with repository data.")
    List<SummaryRepo> summary;

}
