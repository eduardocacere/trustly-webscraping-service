package com.trustly.webscraping.util;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtractServiceUtil {


    public static Optional<Matcher> extract(String page, String regex) {
        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(page);
        return Optional.of(matcher.find())
                .filter(i -> i)
                .map(i -> Optional.of(matcher))
                .orElse(Optional.empty());
    }
}
