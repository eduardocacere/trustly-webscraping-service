package com.trustly.webscraping.proxy;

import com.trustly.webscraping.dto.UrlFileDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "GithubProxy", url = "${web-scraping.host.github}")
public interface GithubProxy {

    @GetMapping("/{repo}")
    String getRepository(@PathVariable("repo") String repo, @RequestParam("page") int page);

    @GetMapping("/{repo}/{name}/find/{branch}")
    String getListFileCode(@PathVariable("repo") String repo, @PathVariable("name") String name, @PathVariable("branch") String branch);

    @GetMapping("{page}")
    String getPage(@PathVariable("page") String page);

    @GetMapping("{path}")
    UrlFileDto getListUrlFile(@PathVariable("path") String path, @RequestHeader("X-Requested-With") String xRequestWith);


}
