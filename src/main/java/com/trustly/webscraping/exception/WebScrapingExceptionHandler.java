package com.trustly.webscraping.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class WebScrapingExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {

        String messageUser = messageSource.getMessage("web-scraping.message.invalid", null, LocaleContextHolder.getLocale());
        String messageDeveloper = ex.getCause() != null ? ex.getCause().toString() : ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(messageUser, messageDeveloper));
        log.error("messageUser: {}, messageDeveloper: {}", messageUser, messageDeveloper);
        return handleExceptionInternal(ex, erros, headers, HttpStatus.BAD_REQUEST, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error("Error MethodArgumentNotValidException");
        List<Erro> erros = createListOfError(ex.getBindingResult());
        return handleExceptionInternal(ex, erros, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({WebScrapingNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<Object> handleNotUpdatException(WebScrapingNotFoundException ex, WebRequest request) {
        
        String messageUser = messageSource.getMessage(ex.getMessage(), null, LocaleContextHolder.getLocale());
        String messageDeveloper = ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(messageUser, messageDeveloper));
        log.error("messageUser: {}, messageDeveloper: {}", messageUser, messageDeveloper);
        return handleExceptionInternal(ex, erros, new HttpHeaders(), HttpStatus.NOT_FOUND , request);
    }

    @ExceptionHandler({WebScrapingException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ResponseEntity<Object> handleWebScrapingException(WebScrapingException ex, WebRequest request) {
        String messageUser = messageSource.getMessage(ex.getMessage(), null, LocaleContextHolder.getLocale());
        String messageDeveloper = ex.getMessage();
        List<Erro> erros = Arrays.asList(new Erro(messageUser, messageDeveloper));
        log.error("messageUser: {}, messageDeveloper: {}", messageUser, messageDeveloper);
        return handleExceptionInternal(ex, erros, new HttpHeaders(), HttpStatus.FORBIDDEN , request);
    }

    private List<Erro> createListOfError(BindingResult bindingResult) {
        List<Erro> erros = new ArrayList<>();

        for(FieldError fieldError : bindingResult.getFieldErrors()) {
            String messageUser = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());
            String messageDeveloper = fieldError.toString();
            erros.add(new Erro(messageUser,messageDeveloper));
            log.error("messageUser: {}, messageDeveloper: {}", messageUser, messageDeveloper);
        }

        return erros;
    }
    
    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Erro {
        
        private String mensagemUser;
        private String mensagemDeveloper;
    }
}
