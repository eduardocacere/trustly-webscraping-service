package com.trustly.webscraping.exception;

@SuppressWarnings("serial")
public class WebScrapingNotFoundException extends RuntimeException  {

    public WebScrapingNotFoundException(String message) {
        super(message);
    }

    public WebScrapingNotFoundException() {
        super("web-scraping.message.not-found");
    }

}
