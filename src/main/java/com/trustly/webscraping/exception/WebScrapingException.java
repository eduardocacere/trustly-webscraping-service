package com.trustly.webscraping.exception;

@SuppressWarnings("serial")
public class WebScrapingException extends RuntimeException  {

    public WebScrapingException(String message) {
        super(message);
    }

    public WebScrapingException() {
        super("web-scraping.message.resource-not-allowed");
    }

}
