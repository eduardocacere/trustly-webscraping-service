package com.trustly.webscraping.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SummaryRepo {
    @Field("_id")
    @Schema(name = "language", description = "Repository Language Name.")
    private String language;

    @Schema(name = "totalProject", description = "Total projects found for language.")
    private Integer totalProject;

    @Schema(name = "totalLines", description = "Total lines of projects.")
    private Integer totalLines;

    @Schema(name = "size", description = "Total Project Size.")
    private BigDecimal size;
}
