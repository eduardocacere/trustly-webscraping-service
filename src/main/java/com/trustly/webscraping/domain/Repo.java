package com.trustly.webscraping.domain;

import com.trustly.webscraping.enums.StatusSync;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document
public class Repo {

    @Id
    private String id;
    private String nameRepository;
    private String nameProject;
    private StatusSync statusSync;
    private String path;
    private String branch;
    private String language;
    private Long codeLine;
    private Double size;
    private List<String> pathFiles;
    private LocalDateTime dateCreate;
}
