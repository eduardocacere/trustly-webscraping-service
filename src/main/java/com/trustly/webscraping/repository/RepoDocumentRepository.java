package com.trustly.webscraping.repository;

import com.trustly.webscraping.domain.Repo;
import com.trustly.webscraping.domain.SummaryRepo;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RepoDocumentRepository extends MongoRepository<Repo, String> {

    Optional<Repo> findByNameProject(String nameProject);
    List<Repo> findByNameRepository(String nameRepository);

    @Aggregation({
            "{$match: { nameRepository: ?0} }",
            "{ $group: {_id: $language, totalLines: { $sum: $codeLine }, size: { $sum: $size}, totalProject: { $sum: 1 } }}"
    })
    List<SummaryRepo> summaryRepo(String nameRepositry);

}
