package com.trustly.webscraping.enums;

public enum StatusSync {

    IN_PROGRESS,
    COMPLETE

}
