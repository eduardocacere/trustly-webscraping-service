package com.trustly.webscraping.resources;

import com.trustly.webscraping.dto.ProgressSyncDto;
import com.trustly.webscraping.dto.SummaryDataDto;
import com.trustly.webscraping.enums.StatusSync;
import com.trustly.webscraping.services.WebScrapingService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/scraping")
@RequiredArgsConstructor
public class WebScrapingResources {

    private final WebScrapingService webScrapingGithubService;
    private static String MESSAGE = "Request received successfully. The repository will be process. This step may take a few minutes.";

    @Operation(summary = "Lists all projects in a given repository", tags = {"repository"},  description = "Lists all projects in a given repository")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
    })
    @GetMapping("/{repo}")
    public ResponseEntity<SummaryDataDto> summary(@PathVariable("repo") String repo) {
        SummaryDataDto summaryData = this.webScrapingGithubService.generateSummary(repo);
        return ResponseEntity.ok(summaryData);
    }

    @Operation(summary = "Load data from repository.", tags = {"repository"}, description = "Load data from repository.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
    })
    @PostMapping("/{repo}")
    public ResponseEntity<ProgressSyncDto> create(@PathVariable("repo") String repo) {
        this.webScrapingGithubService.createDataRepo(repo);

        return ResponseEntity.accepted()
                .body(ProgressSyncDto
                        .builder()
                        .statusSync(StatusSync.IN_PROGRESS)
                        .message(MESSAGE)
                        .build());
    }
}
