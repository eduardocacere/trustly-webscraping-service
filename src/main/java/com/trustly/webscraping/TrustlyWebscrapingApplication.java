package com.trustly.webscraping;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableFeignClients
@EnableAsync
@OpenAPIDefinition(
		info = @Info(
				title = "Trustly Web Scraping",
				version = "1.0",
				description = "Application Web Scraping for Repository"
		)
)
public class TrustlyWebscrapingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrustlyWebscrapingApplication.class, args);
	}

}
